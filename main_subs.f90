module main_subs
!> Defines adios data structure
!! \param adios_group adios group
!! \param my_group_size Stores the adios group size

contains 

subroutine read_parameter_file()

	use main_parameter
  implicit none

  integer, parameter :: IIN = 11
  integer, parameter :: NHEAD = 8
  integer :: ios

  integer :: idummy,i
  character(len=28) :: junk

  open(unit=IIN,file='PAR_FILE',status='old')

  ! ignore header
  do idummy=1,NHEAD
    read(IIN,*)
  enddo

	read(IIN,2) junk, DEBUG
	read(IIN,*)
	read(IIN,*)
	read(IIN,2) junk, CMT_DIR
	read(IIN,2) junk, STATION_DIR
	read(IIN,2) junk, OBSD_DIR
	read(IIN,2) junk, SYNT_DIR
	read(IIN,2) junk, OBSD_FILE_PREFIX
	read(IIN,2) junk, SYNT_FILE_PREFIX
	read(IIN,2) 
	read(IIN,2)  
	read(IIN,3) junk, min_period 
	read(IIN,3) junk, max_period

  close(IIN)

2 format(a,a)
3 format(a,l20)

end subroutine read_parameter_file

subroutine event_list()
  use seismo_variables
  implicit none

  integer, parameter :: IIN = 11
!  integer, parameter :: NHEAD = 12
!  integer ios

  integer :: idummy
  integer  :: i

  open(unit=IIN,file='event_list',status='old')

   read(IIN,*)nevents

   do i = 1, nevents
     read(IIN,'(a)')events(i)
   enddo
   close(IIN)
end subroutine

!> Gets the number of receivers from the STATIONS file
!! \param nreceivers The number of receivers.

subroutine get_nreceivers ()

  use seismo_variables
  implicit none

  integer :: stat

  nreceivers = 0
  open(10, file=STATIONS_FILE, status='old')
  do
    read(10, *, iostat=stat)
    if (stat /= 0) exit
    nreceivers = nreceivers + 1
  end do
  close(10)

end subroutine get_nreceivers

end module main_subs
